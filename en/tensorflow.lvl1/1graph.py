# Habrahabr @ins2718
import tensorflow as tf

# Save the default graph in variable
default_graph = tf.get_default_graph()
# Define const in default graph
const1 = tf.constant(1337.0) # Actually, if value is integer-valued, it must be written with dot, like not integer-valued

# Create empty graph ( second )
second_graph = tf.Graph()
with second_graph.as_default():
    # In this block we are working in second graph
    const2 = tf.constant(13.0)

print( const2.graph is second_graph, const1.graph is second_graph ) # Result : true, false
# Const 2 belong to the second graph, const 1 belong to second graph
print( const2.graph is default_graph, const1.graph is default_graph ) # Result : false, true
# Const 2 belong to the first graph, const 1 belong to the first graph
