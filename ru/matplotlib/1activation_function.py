# https://proglib.io/p/neural-nets-guide/
import matplotlib.pylab as plt
import numpy as np

x = np.arange( -8, 8, 0.1 )  # равномерное распределение от -8 до 8 с шагом 0.1
f = 1 / ( 1 + np.exp(-x) ) # сигмоидальная функция

plt.title("Функция активации f(x) = 1 / (1+exp(-x))") # Заголовок
plt.plot( x, f ) # Диаграмма
plt.xlabel('x') # Подпись оси OX
plt.ylabel('f(x)') # Подпись оси OY
plt.show() # Отобразить