# www.tensorflow.org/api_docs
import tensorflow as tf

# Создаём граф
graph = tf.get_default_graph()

a = tf.constant(1337.01) # Даже если значение целочисленное, оно должно быть записано с точкой, как не целочисленное.
b = tf.constant(1.0)
c = a * b

session = tf.Session() # Открываем сессию

# Выводим тензор "c"
print( session.run(c) )

# Return :
# 1337.01

# Замечание : 
# 2 строка сама создаёт граф по умолчанию, поэтому, в этом случае 
# можно не создавать граф ( 5 строка )