# Habrahabr @ins2718
import tensorflow as tf

default_graph = tf.get_default_graph()
c1 = tf.constant(1.0)

second_graph = tf.Graph()
with second_graph.as_default():
          c2 = tf.constant(2.0)

session = tf.Session() # Open the session
print( c1.eval( session = session ) ) # Result : 1
# print ( c2.eval( session = session ) ) # Error, not the one graph
session.close()

with tf.Session() as session:
          print( c1.eval() ) # We dont need to pass session to eval ; Result : 1

# Use other graph
with tf.Session( graph = second_graph ) as session:         
          print( c2.eval() ) # We dont need to pass session to eval ; Result : 2

# Result :
# 1
# 1
# 2