# www.tensorflow.org/api_docs
import tensorflow as tf

# Build a graph
graph = tf.get_default_graph()

a = tf.constant(1337.01) # Actually, if value is integer-valued, it must be written with dot, like not integer-valued
b = tf.constant(1.0)
c = a * b

# Launch the graph in a session
session = tf.Session() # Open the session

# Evaluate the tensor 'c'
print( session.run(c) )

# Return :
# 1337.01

# Comment : 
# Second string thyself create default graph,
#  In this case we might not create a graph specially 