import matplotlib.pylab as plt
import os

# Функция, получающая количество файлов с папках
def count_files (in_directory): # Возвращает in_directory
          joiner = ( in_directory + os.path.sep ).__add__ 
          return sum (
                    os.path.isfile(filename) # проверка на то, является ли путь файлом
                    for filename
                    in map( joiner, os.listdir( in_directory ) ) # применяем к каждому элементу функцию
          )

mpl = count_files('matplotlib')
#np  = count_files('numpy')
tf  = count_files('tensorflow.lvl1') + count_files('tensorflow.lvl1/2session')
y   = [ mpl, tf ]
x   = [ 'Matplotlib', 'Tensorflow' ]

plt.bar( x, y, color = 'g' )
plt.title('Количество файлов в папках')
plt.show()