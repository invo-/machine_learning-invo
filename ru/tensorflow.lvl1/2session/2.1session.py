# Habrahabr @ins2718
import tensorflow as tf

default_graph = tf.get_default_graph()
c1 = tf.constant(1.0)

second_graph = tf.Graph()
with second_graph.as_default():
          c2 = tf.constant(2.0)

session = tf.Session() # Открываем сессию
print( c1.eval( session = session ) ) # Результат : 1
# print ( c2.eval( session = session ) ) # Ошибка, не тот граф
session.close()

with tf.Session() as session:
          print( c1.eval() ) # Не нужно передавать сессию в eval ; Результат : 1

# Используем другой граф
with tf.Session( graph = second_graph ) as session:         
          print( c2.eval() ) # Не нужно передавать сессию в eval ; Результат : 2

# Вывод :
# 1
# 1
# 2