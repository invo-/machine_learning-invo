# Habrahabr @ins2718
import tensorflow as tf

# Сохраняем граф по умолчанию в переменную
default_graph = tf.get_default_graph()
# Объявляем константу в графе по умолчанию
const1 = tf.constant(1.0) # Даже если значение целочисленное, оно должно быть записано с точкой, как не целочисленное.

# Создаём пустой граф
second_graph = tf.Graph()
with second_graph.as_default():
    # В этом блоке мы работаем во втором графе
    const2 = tf.constant(4.0)

print( const2.graph is second_graph, const1.graph is second_graph ) # Результат : true, false
# Константа 2 принадлежит второму графу, константа 1 принадлежит второму графу
print( const2.graph is default_graph, const1.graph is default_graph ) # Результат : false, true
# Константа 2 принадлежит первому графу, константа 1 принадлежит первому графу