# Useful // Что-то полезное
1. https://www.kaggle.com/learn/machine-learning
2. www.tensorflow.org/tutorials - Tensorflow tutorials // Руководства по tensorflow.
3. www.deeplearningbook.org - About deep learning // Книга по глубокому обучению.
4. https://keras.io/ - Keras documentation // Документация по Keras.
5. https://habr.com/
6. www.codewars.com - This site can help you with learning python. // Этот сайт может помочь вам в изучении питона с помощью решения задач.
7. "AByteofPython" // Книга "AByteofPython" русскоязычное издание
8. Книга "Глубокое обучение. Погружение в мир нейронных сетей" С.Николенко, А.Кадурин, Е.Архангельская. // Russian-language book
9. Книга "Статистика и котики" Владимир Савельев // Russian-language book
# Which tools i used in this repository // Какие инструменты я использовал в этом репозитории
* Python
* Tensorflow
* Keras
* Pandas
* matplotlib

# Instructions
1. If you will use this repository for teach machine learning - please, view files in order ( for example : 1graph.py -> 2session( 2session.py -> 2.1session.py ) )

# Инструкции
1. Если вы используете этот репозиторий для изучения машинного обучения - пожалуйста, просматривайте файлы по порядку ( пример : 1graph.py -> 2session( 2session.py -> 2.1session.py ) )

#### Some examples in this repository i took from different resouces ( sites, books, videos )
#### Некоторые примеры в этом репозитории я брал из различных ресурсов ( сайты, книги, видео )
